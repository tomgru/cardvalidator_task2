package com.CardValidator;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CardValidatorServlet
 */
@WebServlet("/CardValidatorServlet")
public class CardValidatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CardValidatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	boolean validateCcNumber(String data){
		return (data != null && data.length() > 0 && data.matches("[0-9]+")) ? true : false;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String ccNumberInputText = (String)request.getParameter("ccNumberInputText");
		request.setAttribute("ccNumberInputText", ccNumberInputText);
		
		if(validateCcNumber(ccNumberInputText)){
			request.setAttribute("validationCheckInputText", true);
		}
		else request.setAttribute("validationCheckInputText", false);
		
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
