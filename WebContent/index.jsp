<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Card Validator</title>
</head>
<body>
	<form action="CardValidatorServlet" method="post">
	CC number:
	<%	
	String ccNumber = (String)request.getAttribute("ccNumberInputText");
	if(ccNumber != null) {
		%>
		<input type="text" name="ccNumberInputText" value="<%=ccNumber%>">		
		<%
	} else {
		%>
		<input type="text" name="ccNumberInputText" value="">		
		<%
	}
	%>
	<input type="submit" value="AJAX check">
	Validation check:
	<%
	Boolean validationCheck = (Boolean)request.getAttribute("validationCheckInputText");
	if(validationCheck != null) {
		%>
		<input type="text" name="validationCheckInputText" value="<%=(validationCheck) ? "OK" : "ERROR"%>">
		<%
	} else {
		%>
		<input type="text" name="validationCheckInputText" value="">
		<%
	}
	%>
	</form>
</body>
</html>